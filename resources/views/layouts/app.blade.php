<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CSV to DB</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
		<nav class="flex items-center justify-between flex-wrap bg-primary-500 p-6">
			<a href="/" class="flex items-center flex-shrink-0 text-white mr-6">
				<svg class="fill-current h-8 w-8 mr-2">
					<path d="M16 0c-8.837 0-16 2.239-16 5v4c0 2.761 7.163 5 16 5s16-2.239 16-5v-4c0-2.761-7.163-5-16-5z"></path>
					<path d="M16 17c-8.837 0-16-2.239-16-5v6c0 2.761 7.163 5 16 5s16-2.239 16-5v-6c0 2.761-7.163 5-16 5z"></path>
					<path d="M16 26c-8.837 0-16-2.239-16-5v6c0 2.761 7.163 5 16 5s16-2.239 16-5v-6c0 2.761-7.163 5-16 5z"></path>
				</svg>
				<span class="font-semibold text-xl tracking-tight">CSV to DB</span>
			</a>
			<div class="block md:hidden">
				<button @click="navbarVisible = !navbarVisible" class="flex items-center px-3 py-2 border rounded text-primary-200 border-primary-400 hover:text-white hover:border-white">
					<svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>{{ __('Menu') }}</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
				</button>
			</div>
			<div class="w-full flex-grow md:flex md:items-center md:w-auto" :class="navbarVisible ? 'block' : 'hidden'">
				<div class="text-md md:flex-grow">
					@auth
						<a href="{{ route('users.index') }}" class="block mt-4 md:inline-block md:mt-0 {{request()->is('users') ? 'text-white' : 'text-primary-200'}} hover:text-white mr-4">
							{{ __('Users') }}
						</a>
						<a href="{{ route('posts.index') }}" class="block mt-4 md:inline-block md:mt-0 {{request()->is('posts') ? 'text-white' : 'text-primary-200'}} hover:text-white mr-4">
							{{ __('Posts') }}
						</a>
						<a href="{{ route('comments.index') }}" class="block mt-4 md:inline-block md:mt-0 {{request()->is('comments') ? 'text-white' : 'text-primary-200'}} hover:text-white mr-4">
							{{ __('Comments') }}
						</a>
						<a href="{{ route('todos.index') }}" class="block mt-4 md:inline-block md:mt-0 {{request()->is('todos') ? 'text-white' : 'text-primary-200'}} hover:text-white">
							{{ __('Todos') }}
						</a>
					@endauth
				</div>
				<div class="flex flex-grow items-center justify-end">
					@guest
						<a href="{{ route('login') }}" class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-primary-500 hover:bg-white mr-2">{{ __('Login') }}</a>
						@if (Route::has('register'))
							<a href="{{ route('register') }}" class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-primary-500 hover:bg-white">{{ __('Register') }}</a>
						@endif
					@else
						<a href="{{ route('home') }}" class="flex items-center text-sm leading-none px-2 py-1 rounded text-white border-white hover:border-transparent hover:text-primary-500 hover:bg-white mr-2">
							<img src="{{ Auth::user()->avatar(24) }}" class="h-full mr-1 rounded-full" alt="User Avatar">
							{{ Auth::user()->name }}
						</a>
						<a href="{{ route('logout') }}"  class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-primary-500 hover:bg-white mr-2"
						onclick="event.preventDefault();
								document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					@endguest
				</div>
			</div>
		</nav>
		{{-- <nav class="flex items-center justify-between flex-wrap bg-primary-500 p-6">
			<div class="flex items-center justify-start">
				<a href="{{ url('/') }}" class="flex items-center flex-shrink-0 text-white mr-6">
					<svg class="fill-current h-8 w-8 mr-2">
						<path d="M16 0c-8.837 0-16 2.239-16 5v4c0 2.761 7.163 5 16 5s16-2.239 16-5v-4c0-2.761-7.163-5-16-5z"></path>
						<path d="M16 17c-8.837 0-16-2.239-16-5v6c0 2.761 7.163 5 16 5s16-2.239 16-5v-6c0 2.761-7.163 5-16 5z"></path>
						<path d="M16 26c-8.837 0-16-2.239-16-5v6c0 2.761 7.163 5 16 5s16-2.239 16-5v-6c0 2.761-7.163 5-16 5z"></path>
					</svg>
					<span class="font-semibold text-xl tracking-tight">CSV to DB</span>
				</a>
				<a href="#">Somewhere</a>
				<a href="#">Somewhere</a>
				<a href="#">Somewhere</a>
				<a href="#">Somewhere</a>
				<a href="#">Somewhere</a>
			</div>
			<div class="flex flex-grow items-center justify-end">
				@guest
					<a href="{{ route('login') }}" class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-primary-500 hover:bg-white mr-2">{{ __('Login') }}</a>
					@if (Route::has('register'))
						<a href="{{ route('register') }}" class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-primary-500 hover:bg-white">{{ __('Register') }}</a>
					@endif
				@else
					<a href="{{ route('home') }}" class="flex items-center text-sm leading-none px-2 py-1 rounded text-white border-white hover:border-transparent hover:text-primary-500 hover:bg-white mr-2">
						<img src="{{ Auth::user()->avatar(24) }}" class="h-full mr-1 rounded-full" alt="User Avatar">
						{{ Auth::user()->name }}
					</a>
					<a href="{{ route('logout') }}"  class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-primary-500 hover:bg-white mr-2"
					onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				@endguest

			</div>
		</nav> --}}

        <main class="py-4 container mx-auto">
            @yield('content')
		</main>

		<footer class="text-center text-gray-500 text-xs">
			&copy;2020 <a target="__blank" rel="noopener" class="text-primary-500 hover:text-primary-700 font-bold" href="https://goofies.gr">Goofies'</a>. All rights reserved.
		</footer>
    </div>
</body>
</html>
