@extends('layouts.app')

@section('content')
@if (session('status'))
	<div class="mb-4">
		<x-alert>{{ session('status') }}</x-alert>
	</div>
@endif

<div class="flex flex-wrap -mx-2">
	<div class="w-1/3 px-2 mb-2">
		<select-input v-model="tableToInsert" name="table" label="{{ __('Table') }}">
			@foreach ($availableTables as $table)
				<option class="capitalize" value="{{ $table }}">{{ $table }}</option>
			@endforeach
		</select-input>
	</div>
	<div class="w-2/3 px-2 mb-2">
		<file-input @changed='fileData = $event' name="file" label="{{ __('Choose a file') }}" placeholder="{{ __('Choose a file') }}"></file-input>
	</div>
	<div class="w-1/3 px-2 mb-2">
		<select-input v-model='delimeterType' name="delimeter" label="{{ __('Delimiter') }}">
			<option value="auto">{{ __('Auto') }}</option>
			<option value="comma">{{ __('Comma') }}</option>
			<option value="tab">{{ __('Tab') }}</option>
		</select-input>
	</div>
	<div class="w-1/3 px-2 mb-2">
		<select-input v-model='decimalSign' name="decimal-sign" label="{{ __('Decimal Sign') }}">
			<option value="dot">{{ __('Dot') }}</option>
			<option value="comma">{{ __('Comma') }}</option>
		</select-input>
	</div>
	<div class="w-1/3 px-2 mb-2 self-end">
		<check-box v-model="headersIncluded" name="include-headers">{{ __('File Includes Headers') }}</check-box>
	</div>
	<div class="w-full px-2 mb-2" v-if="tableDetails && csvHeaders && csvObjects">
		<table-component 
			@update:column-match="columnMatch = $event"
			@update:default-values="defaultValues = $event"
			:failed-records="failedRecords"
			:succeeded-records="succeededRecords"
			:csv-objects="csvObjects"
			:table-details="tableDetails"
			:csv-headers="csvHeaders"></table-component>
	</div>
	<div v-if="csvObjects.length" class="w-full w-full px-2 mb-2">
		<button v-if="!processing" @click="insertToDatabase" class="w-full bg-teal-500 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
			{{ __('Insert Data to Database') }}
		</button>
		<button v-else class="w-full bg-teal-200 text-white font-bold py-2 px-4 rounded cursor-not-allowed flex items-center justify-center">
			<span class="mr-3">
				{{ __('Processing') }}
			</span>
			<div class="flex items-center justify-between">  
				<div class="w-3 h-3 mr-1 bg-teal-400 rounded-full pulse-bubble-1"></div>
				<div class="w-3 h-3 mr-1 bg-teal-400 rounded-full pulse-bubble-2"></div>
				<div class="w-3 h-3 mr-1 bg-teal-400 rounded-full pulse-bubble-3"></div>
			</div>
		</button>
	</div>
	<div class="w-full px-2 mb-2" v-if="errorMessages.length" id="results">
		<div class="w-full md:w-2/3 lg:w-2/4 mx-auto bg-gray-100 rounded shadow py-3 px-10 overflow-auto" style="max-height: 500px">
			<h2 class="text-center text-3xl mb-4">Errors during the process</h2>
			<ul class="list-disc" v-for='error in errorMessages'>
				<li>Row #@{{error.id + 1}} has failed:</li>
				<ul class="list-circle pl-3">
					<li v-for='message in error.errors'>
						 @{{message[0]}}
					</li>
				</ul>
			</ul>
		</div>
	</div>
</div>

@endsection
