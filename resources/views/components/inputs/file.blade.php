@props([
	'type' => 'text',
	'name',
	'label',
	'placeholder',
	'required' => false,
])

<div>
	<label class="block text-gray-700 text-sm font-bold mb-2" for="{{ $name }}">
		{{ $label }}
	</label>
	<label for="{{ $name }}" class="block shadow appearance-none border @error($name) border-red-500 @enderror rounded w-full py-2 px-3 text-gray-700 leading-tight file-input-placeholder">
		<span class="text-gray-500">{{ $placeholder }}</span>
	</label>
	<input type="file" class="hidden"
		id="{{ $name }}" 
		name="{{ $name }}"
		@if ($required) required @endif>
	@error($name)
		<p class="text-red-500 text-xs italic">{{ $message }}</p>
	@enderror
</div>