@props([
	'type' => 'text',
	'name',
	'label',
	'placeholder',
	'required' => false,
	'autofocus' => false,
	'autocomplete' => false,
	'noOld' => false,
	'value' => false
])

<div>
	<label class="block text-gray-700 text-sm font-bold mb-2" for="{{ $name }}">
		{{ $label }}
	</label>
	<input 
		class="shadow appearance-none border @error($name) border-red-500 @enderror rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
		id="{{ $name }}" 
		name="{{ $name }}" 
		type="{{ $type }}" 
		placeholder="{{ $placeholder }}" 
		@if ($value) value="{{ $value }}"
		@elseif (!$noOld) value="{{ old($name) }}" 
		@endif
		@if ($required) required @endif  
		@if ($autocomplete) autocomplete="{{ $autocomplete }}" @endif  
		@if ($autofocus) autofocus @endif>
	@error($name)
		<p class="text-red-500 text-xs italic">{{ $message }}</p>
	@enderror
</div>