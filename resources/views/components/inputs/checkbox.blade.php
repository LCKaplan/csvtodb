@props([
	'required' => false,
	'name',
	'label',
])

<div>
	<label class="flex justify-start items-center text-gray-500 font-bold">
		<input class="leading-tight mr-2" type="checkbox"
			name="{{ $name }}" 
			id="{{ $name }}"
			@if (old($name)) checked @endif
			@if ($required) required @endif>
		<span class="text-sm">
			{{ $label }}
		</span>
	</label>
	@error($name)
		<p class="text-red-500 text-xs italic">{{ $message }}</p>
	@enderror
</div>