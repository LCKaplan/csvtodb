@extends('layouts.app')

@section('content')
<div class="w-full max-w-sm mx-auto pt-5">
	<form class="bg-white shadow-md rounded border px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('password.update') }}">
		<h1 class="text-center text-3xl font-semibold mb-5">
			{{ __('Reset Password') }}
		</h1>
		@csrf
		<input type="hidden" name="token" value="{{ $token }}">
		<div class="mb-4">
			<x-inputs.input type="email" name="email" label="{{ __('E-Mail Address') }}" placeholder="Email" required autocomplete="email" value="{{ $email ?? old('email') }}"></x-inputs.input>
		</div>
		<div class="mb-4">
			<x-inputs.input type="password" name="password" label="{{ __('Password') }}" placeholder="**********" required autocomplete="new-password" no-old></x-inputs.input>
		</div>
		<div class="mb-6">
			<x-inputs.input type="password" name="password_confirmation" label="{{ __('Confirm Password') }}" placeholder="**********" required autocomplete="new-password" no-old></x-inputs.input>
		</div>
		<button class="w-full bg-primary-500 hover:bg-primary-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
			{{ __('Reset Password') }}
		</button>
	</form>
</div>
@endsection
