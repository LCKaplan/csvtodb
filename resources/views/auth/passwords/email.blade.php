@extends('layouts.app')

@section('content')
<div class="w-full max-w-sm mx-auto pt-5">
	@if (session('status'))
		<div class="mb-4">
			<x-alert>{{ session('status') }}</x-alert>
		</div>
	@endif

	<form class="bg-white shadow-md rounded border px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('password.email') }}">
		<h1 class="text-center text-3xl font-semibold mb-5">
			{{ __('Reset Password') }}
		</h1>
		@csrf
		<div class="mb-4">
			<x-inputs.input type="email" name="email" label="{{ __('E-Mail Address') }}" placeholder="Email" required autocomplete="email" autofocus></x-inputs.input>
		</div>
		<button class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
			{{ __('Send Password Reset Link') }}
		</button>
	</form>
</div>
@endsection
