@extends('layouts.app')

@section('content')
<div class="w-full max-w-sm mx-auto pt-5">
	<form class="bg-white shadow-md rounded border px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('register') }}">
		<h1 class="text-center text-3xl font-semibold mb-5">
			{{ __('Register') }}
		</h1>
		@csrf
		<div class="mb-4">
			<x-inputs.input name="name" label="{{ __('Name') }}" placeholder="{{ __('Name') }}" required autocomplete="name" autofocus></x-inputs.input>
		</div>
		<div class="mb-4">
			<x-inputs.input name="lastname" label="{{ __('Lastname') }}" placeholder="{{ __('Lastname') }}" required autocomplete="lastname" autofocus></x-inputs.input>
		</div>
		<div class="mb-4">
			<x-inputs.input type="email" name="email" label="{{ __('E-Mail Address') }}" placeholder="{{ __('E-Mail Address') }}" required autocomplete="email"></x-inputs.input>
		</div>
		<div class="mb-4">
			<x-inputs.input type="password" name="password" label="{{ __('Password') }}" placeholder="**********" required autocomplete="new-password" no-old></x-inputs.input>
		</div>
		<div class="mb-6">
			<x-inputs.input type="password" name="password_confirmation" label="{{ __('Confirm Password') }}" placeholder="**********" required autocomplete="new-password" no-old></x-inputs.input>
		</div>
		<button class="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
			{{ __('Register') }}
		</button>
	</form>
</div>
@endsection
