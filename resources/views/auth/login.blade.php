@extends('layouts.app')

@section('content')
<div class="w-full max-w-sm mx-auto pt-5">
	<form class="bg-white shadow-md rounded border px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('login') }}">
		<h1 class="text-center text-3xl font-semibold mb-5">
			{{ __('Login') }}
		</h1>
		@csrf
		<div class="mb-4">
			<x-inputs.input type="email" name="email" label="{{ __('E-Mail Address') }}" placeholder="Email" required autofocus autocomplete="email"></x-inputs.input>
		</div>
		<div class="mb-4">
			<x-inputs.input type="password" name="password" label="{{ __('Password') }}" placeholder="**********" required autocomplete="currrent-password" no-old></x-inputs.input>
		</div>
		<div class="mb-6">
			<x-inputs.checkbox name="remember" label="{{ __('Remember Me') }}"></x-inputs.checkbox>
		</div>
		<div class="flex items-center justify-between">
			<button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
				{{ __('Login') }}
			</button>
			@if (Route::has('password.request'))
				<a class="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800" href="{{ route('password.request') }}">
					{{ __('Forgot Password?') }}
				</a>
			@endif
		</div>
	</form>
</div>
@endsection
