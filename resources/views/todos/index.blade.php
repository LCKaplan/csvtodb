@extends('layouts.app')

@section('content')
<h1 class="text-center text-3xl font-semibold mb-2">
	{{ __('Todos') }}
</h1>
@if ($todos->isEmpty())
	<h1 class="text-center text-2xl font-semibold mb-2">
		{{ __('There is no records.') }}
	</h1>
@else
	<div class="w-full overflow-auto p-2" style="max-height: calc(100vh - 200px)">
		<table class="table-auto min-w-full">
			<thead>
				<tr>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('#') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('User ID') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Title') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Completed') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Created At') }}</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($todos as $todo)
					<tr @if ($loop->even) class="bg-gray-100" @endif>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $todo->id }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $todo->user_id }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $todo->title }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $todo->comleted ? __('Completed') : __('Incomplete') }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $todo->created_at->format('d-m-Y H:i:s') }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endif
@endsection