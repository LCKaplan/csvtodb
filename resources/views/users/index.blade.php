@extends('layouts.app')

@section('content')
<h1 class="text-center text-3xl font-semibold mb-2">
	{{ __('Users') }}
</h1>
@if ($users->isEmpty())
	<h1 class="text-center text-2xl font-semibold mb-2">
		{{ __('There is no records.') }}
	</h1>
@else
	<div class="w-full overflow-auto p-2" style="max-height: calc(100vh - 200px)">
		<table class="table-auto min-w-full">
			<thead>
				<tr>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Name') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Lastname') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Email') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Phone') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Mobile') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Password') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Created At') }}</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($users as $user)
					<tr @if ($loop->even) class="bg-gray-100" @endif>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $user->name }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $user->lastname }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $user->email }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $user->phone }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $user->mobile }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $user->password }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $user->created_at->format('d-m-Y H:i:s') }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endif
@endsection