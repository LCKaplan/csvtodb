@extends('layouts.app')

@section('content')
<h1 class="text-center text-3xl font-semibold mb-2">
	{{ __('Posts') }}
</h1>
@if ($posts->isEmpty())
	<h1 class="text-center text-2xl font-semibold mb-2">
		{{ __('There is no records.') }}
	</h1>
@else
	<div class="w-full overflow-auto p-2" style="max-height: calc(100vh - 200px)">
		<table class="table-auto min-w-full">
			<thead>
				<tr>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('#') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('User ID') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Title') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Body') }}</th>
					<th class="whitespace-no-wrap px-4 py-2">{{ __('Created At') }}</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($posts as $post)
					<tr @if ($loop->even) class="bg-gray-100" @endif>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $post->id }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $post->user_id }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $post->title }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $post->body }}</td>
						<td class="whitespace-no-wrap border px-4 py-2">{{ $post->created_at->format('d-m-Y H:i:s') }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endif
@endsection