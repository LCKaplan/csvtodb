/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Axios from 'axios';

const app = new Vue({
	el: '#app',
	data: {
		navbarVisible: false,
		fileData: null,
		csvRaw: null,
		delimeterType: null,
		decimalSign: null,
		headersIncluded: null,
		tableToInsert: null,
		processing: false,
		succeededRecords: 0,
		failedRecords: 0,
		tableDetails: [],
		columnMatch: [],
		defaultValues: [],
		csvObjects: [],
		errorMessages: [],
	},
	computed: {
		columnDelimiter() {
			if (this.delimeterType === "comma") {
				return ","
			} else if (this.delimeterType === "tab") {
				return "\t"
			} else if(this.csvRaw) {
				//test for delimiter
				
				//count the number of commas
				let RE = new RegExp("[^,]", "gi");
				let numCommas = this.csvRaw.replace(RE, "").length;
			
				//count the number of tabs
				RE = new RegExp("[^\t]", "gi");
				let numTabs = this.csvRaw.replace(RE, "").length;
			
				//set delimiter
				return numTabs > numCommas ? "\t" : ",";
			} else {
				return ","
			}
		},
		csvArray() {
			//Return null if file is not selected
			if(!this.csvRaw) return null;
			
			// Check to see if the delimiter is computed. If not,
			// then default to comma.
			let strDelimiter = (this.columnDelimiter || ",");
	  
			// Create a regular expression to parse the CSV values.
			var objPattern = new RegExp(
				(
					"(\\" + strDelimiter + "|\\r?\\n|\\r|^)" + // Delimiters.
					"(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" + // Quoted fields.
					"([^\"\\" + strDelimiter + "\\r\\n]*))" // Standard fields.
				),
				"gi"
			);
	  
			// Create an array to hold our data. Give the array
			// a default empty first row.
			var arrData = [[]];
	  
			// Create an array to hold our individual pattern
			// matching groups.
			var arrMatches = null;
	  
			// Keep looping over the regular expression matches
			// until we can no longer find a match.
			while (arrMatches = objPattern.exec( this.csvRaw )){
	  
				// Get the delimiter that was found.
				var strMatchedDelimiter = arrMatches[ 1 ];
		
				// Check to see if the given delimiter has a length
				// (is not the start of string) and if it matches
				// field delimiter. If id does not, then we know
				// that this delimiter is a row delimiter.
				if (strMatchedDelimiter.length && strMatchedDelimiter != strDelimiter){
		
					// Since we have reached a new row of data,
					// add an empty row to our data array.
					arrData.push( [] );
		
				}
		
				// Now that we have our delimiter out of the way,
				// let's check to see which kind of value we
				// captured (quoted or unquoted).
				if (arrMatches[ 2 ]){
		
					// We found a quoted value. When we capture
					// this value, unescape any double quotes.
					var strMatchedValue = arrMatches[ 2 ].replace(
						new RegExp( "\"\"", "g" ),
						"\""
					);
		
				} else {
		
					// We found a non-quoted value.
					var strMatchedValue = arrMatches[ 3 ];
		
				}
		
				// Now that we have our value string, let's add
				// it to the data array.
				arrData[ arrData.length - 1 ].push( strMatchedValue );
			}
			
			let isDecimal_re = /^\s*(\+|-)?((\d+([,\.]\d+)?)|([,\.]\d+))\s*$/;
			//escape out any tabs or returns or new lines
			arrData = arrData.map(row => {
				return row.map(column  => {
						//replace comma with dot if comma is decimal separator
						if(this.decimalSign='comma' && isDecimal_re.test(column)){
							column = column.replace(",", ".");
						}

						return column.replace("\t", "\\t").replace("\n", "\\n").replace("\r", "\\r")
					})
			})
	  
			// Return the parsed data.
			return arrData;

		},
		csvHeaders() {
			if(!this.csvArray) return null;

			if (this.headersIncluded) return this.csvArray[0];
			else return this.csvArray[0].map((column, index) => "val "+String(index + 1))
		},
		csvRows() {
			if(!this.csvArray) return [];

			if (this.headersIncluded) return this.csvArray.slice(1);
			else return this.csvArray;
		},
		csvColumnTypes() {
			if(!this.csvRows) return null;

			let types = ['string', 'int', 'float']

			return this.csvRows.reduce((acc, cRow) => {
					return cRow.map((column, index) => {
						let prevType = acc[index];

						if(prevType == 'string') return 'string';
						if(column == '' && types.indexOf(prevType) == -1) return '';

						if(column != '' && isNaN(new Number(column))) {
							return 'string';
						} else if(String(column).indexOf(".") > 0 || prevType == 'float') {
							return 'float';
						} else if(prevType != 'float'){
							return 'int';
						} else {
							return ''
						}
					})
				}, []);
		},
		parseError() {
			if(!this.csvRows) return false;
			let totalColumns = this.csvHeaders.length;

			let error = '';

			this.csvRows.forEach((row, index) => {
				if(row.length != totalColumns) {
					error += `Error parsing row ${index}. Wrong number of columns.`;
				}
			});

			return error;
		}
	},
	watch: {
		fileData() {
			this.resetProcessedData();
			if(this.fileData) {
				if (window.FileReader) {
					let reader = new FileReader();
					reader.readAsText(this.fileData);

					reader.onload = event => {
						let input = event.target.result

						let rowDelimiter = "\n";
						// kill extra empty lines
						let RE = new RegExp("^" + rowDelimiter + "+", "gi");
						input = input.replace(RE, "");
						RE = new RegExp(rowDelimiter + "+$", "gi");
						input = input.replace(RE, "");

						this.csvRaw = input;
					};
	
					reader.onerror = event => {
						if(event.target.error.name == "NotReadableError") {
							alert("Cannot read file!");
							this.csvRaw = null;
						}
					};
				} else {
					alert('FileReader is not supported in this browser.');
				}
			} else {
				this.csvRaw = null;
			}
		},
		tableToInsert() {
			this.resetProcessedData();
			if(this.tableToInsert) {
				Axios.get(`/tableDetails/${this.tableToInsert}`)
					.then(res => this.tableDetails = res.data)
					.catch(error => this.tableDetails = null);
			} else {
				this.tableDetails = null
			}
		},
		csvRows() {
			this.resetProcessedData();
		},
		tableDetails() {
			this.resetProcessedData();
		},
		columnMatch() {
			this.resetProcessedData();
		},
		defaultValues() {
			this.resetProcessedData();
		}
	},
	methods: {
		insertToDatabase() {
			this.processing = true;

			let totalCalls = this.csvObjects.length;
			this.succeededRecords = 0;
			this.failedRecords = 0;
			this.errorMessages = [];

			this.csvObjects.forEach((item, index) => {
				setTimeout(() => {
					Axios.post(`/${this.tableToInsert}`, item)
						.then(res => {
							this.succeededRecords++
							item.dbStatus = 'success';
						})
						.catch(err => {
							this.failedRecords++
							item.dbStatus = 'error';
							this.errorMessages.push({
								id: index,
								errors: err.response.data.errors
							});
						}).then(() => {
							if(this.succeededRecords + this.failedRecords == totalCalls) this.processing = false;
						})
				}, 500 * index)
			})
		},
		updateCsvObjects() {
			this.csvObjects = this.csvRows.map(row => {
				let obj = {};
				this.tableDetails.forEach((column, index) => {
					obj.dbStatus = '';
					let columnMatch = this.columnMatch[index]
					if(columnMatch == 'setDefaultValue') {
						console.log(this.defaultValues, index);
						obj[column.name] = this.defaultValues[index];
					} else {
						obj[column.name] = row[columnMatch];
					}
				})
				return obj;
			})
		},
		resetProcessedData() {
			this.updateCsvObjects();
			this.errorMessages = [];
			this.failedRecords = 0;
			this.succeededRecords = 0
		}
	}
});
