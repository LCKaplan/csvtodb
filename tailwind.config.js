const { colors } = require('tailwindcss/defaultTheme')

module.exports = {
	purge: [
		'./resources/**/*.blade.php',
		'./resources/**/*.vue',
	],
	theme: {
		colors: {
		  primary: colors.blue,
		  secondary: colors.yellow,
		  neutral: colors.gray,
		  ...colors
		},
		listStyleType: {
			none: 'none',
			disc: 'disc',
			circle: 'circle',
		}
	  }
}