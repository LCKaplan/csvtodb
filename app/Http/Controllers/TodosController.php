<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{
	public function create(Request $request)
	{
		$request->validate([
            'user_id' => ['required', 'integer', 'exists:users,id'],
			'title' => ['required', 'string', 'max:255'],
			'completed' => ['boolean'],
		]);
		
		$todo = Todo::create([
			'user_id' => request('user_id'),
			'title' => request('title'),
			'completed' => request('completed'),
		]);

		return $todo;
	}

	public function index()
	{
		return view('todos.index', ['todos' => Todo::all()]);
	}
}
