<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Propaganistas\LaravelPhone\PhoneNumber;

class UsersController extends Controller
{
	public function create(Request $request)
	{
		$request->validate([
            'name' => ['required', 'string', 'max:255'],
			'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'phone' => ['phone:GR'],
			'mobile' => ['phone:GR'],
            'password' => ['required', 'string', 'min:8'],
		]);
		
		$user = User::create([
			'name' => request('name'),
			'lastname' => request('lastname'),
			'email' => request('email'),
			'phone' => PhoneNumber::make(request('phone'), 'GR')->formatE164(),
			'mobile' => PhoneNumber::make(request('mobile'), 'GR')->formatE164(),
			'password' => Hash::make(request('password'))
		]);

		return $user;
	}

	public function index()
	{
		return view('users.index', ['users' => User::all()]);
	}
}
