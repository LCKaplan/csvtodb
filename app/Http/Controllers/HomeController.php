<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', [
			'availableTables' => ['users', 'posts', 'comments', 'todos']
		]);
	}
	
	public function tableDetails($table)
	{
		$tables = [
			'users' => [
				[
					'name' => 'name',
					'required' => true,
				],
				[
					'name' => 'lastname',
					'required' => true,
				],
				[
					'name' => 'email',
					'required' => true,
				],
				[
					'name' => 'phone',
				],
				[
					'name' => 'mobile',
				],
				[
					'name' => 'password',
					'required' => true,
				]
			],
			'posts' => [
				[
					'name' => 'user_id',
					'required' => true
				],
				[
					'name' => 'title',
					'required' => true
				],
				[
					'name' => 'body',
					'required' => true
				]
			],
			'comments' => [
				[
					'name' => 'post_id',
					'required' => true
				],
				[
					'name' => 'name',
					'required' => true
				],
				[
					'name' => 'email',
					'required' => true
				],
				[
					'name' => 'body',
					'required' => true
				]
			],
			'todos' => [
				[
					'name' => 'user_id',
					'required' => true
				],
				[
					'name' => 'title',
					'required' => true
				],
				[
					'name' => 'completed',
					'boolean' => true
				]
			]
		];

		if(!isset($tables[$table])) {
			return response()->json(['message' => __('Table not found')], 404);
		}

		return $tables[$table];
	}

	
}
