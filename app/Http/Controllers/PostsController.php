<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
	public function create(Request $request)
	{
		$request->validate([
            'user_id' => ['required', 'integer', 'exists:users,id'],
			'title' => ['required', 'string', 'min:10', 'max:255'],
            'body' => ['required', 'string', 'min:10'],
		]);
		
		$post = Post::create([
			'user_id' => request('user_id'),
			'title' => request('title'),
			'body' => request('body'),
		]);

		return $post;
	}

	public function index()
	{
		return view('posts.index', ['posts' => Post::all()]);
	}
}
