<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
	public function create(Request $request)
	{
		$request->validate([
            'post_id' => ['required', 'integer', 'exists:posts,id'],
			'name' => ['required', 'string', 'max:255'],
			'email' => ['required', 'string', 'email', 'max:255'],
            'body' => ['required', 'string', 'min:10'],
		]);
		
		$comment = Comment::create([
			'post_id' => request('post_id'),
			'name' => request('name'),
			'email' => request('email'),
			'body' => request('body'),
		]);

		return $comment;
	}

	public function index()
	{
		return view('comments.index', ['comments' => Comment::all()]);
	}
}
