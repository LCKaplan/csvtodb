<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::middleware('auth')->group(function() {
	Route::get('/', 'HomeController@index');
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/tableDetails/{table}', 'HomeController@tableDetails')->name('table-details');

	Route::post('/users', 'UsersController@create')->name('users.create');
	Route::get('/users', 'UsersController@index')->name('users.index');

	Route::post('/posts', 'PostsController@create')->name('posts.create');
	Route::get('/posts', 'PostsController@index')->name('posts.index');

	Route::post('/comments', 'CommentsController@create')->name('comments.create');
	Route::get('/comments', 'CommentsController@index')->name('comments.index');

	Route::post('/todos', 'TodosController@create')->name('todos.create');
	Route::get('/todos', 'TodosController@index')->name('todos.index');
});
