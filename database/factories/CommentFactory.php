<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
		'post_id' => factory(App\Post::class),
		'name' => $faker->name(),
		'email' => $faker->safeEmail(),
		'body' => $faker->paragraph(4)
	];
});
