<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Todo;
use Faker\Generator as Faker;

$factory->define(Todo::class, function (Faker $faker) {
    return [
		'user_id' => factory(App\User::class),
		'title' => $faker->sentence(),
		'completed' => $faker->boolean()
    ];
});
